# Debugging 

Recommended `RUST_LOG` value:

``` shell
export RUST_LOG="turtle_reborn=debug,matrix_sdk_base=error"
```
