<a name="unreleased"></a>
## [Unreleased]

### Features
- Add basic logging framework
- Set display name on startup


<a name="0.0.2"></a>
## [0.0.2] - 2021-10-24
### Code Refactoring
- Add get_hierarchy method to turtle
- Move format_node into space_walker

### Features
- Improve space directory logic
- Use directory for display_space
- Add space_directory interface
- Switch to native ssl library


<a name="0.0.1"></a>
## 0.0.1 - 2021-10-20
### Bug Fixes
- Use full state sync

### Features
- Improve space formatting
- display_space command
- Only try to join public rooms
- Automatically join rooms in the specified spaces
- Add accessors to Turtle
- Add Turtle::get_room_id
- Add timer scaffolding
- Add get_display_name method
- Basic command processing
- Reduce log level in lru_time_cache
- Add verification binary
- Add turtle::get_state with caching
- Add get_state_raw to turtle
- Basic cache structure
- Display space
- Parallel space exploration
- basic space explorer
- add basic autojoin
- bootstrap crosssigning
- Log bot in
- Read config file
- Add store directory
- Add homesever and username to config
- Add credentials to config
- Basic config generator
- Some basic configuration items


[Unreleased]: https://gitea.community.rs/Community/Turtle-Reborn/compare/0.0.2...HEAD
[0.0.2]: https://gitea.community.rs/Community/Turtle-Reborn/compare/0.0.1...0.0.2
