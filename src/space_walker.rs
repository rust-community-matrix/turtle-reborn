//! Utilities for walking a space
use std::{collections::HashSet, convert::TryFrom, sync::Arc};

use eyre::{Result, WrapErr};
use futures::{
    future::{join_all, BoxFuture},
    FutureExt,
};
use matrix_sdk::ruma::{events::AnyStateEvent, RoomId};
use tokio::{sync::RwLock, task};
use tracing::{info_span, instrument, trace, Instrument};

use crate::Turtle;

/// Node structure
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
pub enum Node {
    /// Non-space room
    Room(RoomId),
    /// Room that is also a space
    Space {
        /// [`RoomId`] of the space
        space: RoomId,
        /// [`RoomId`]s of the space's children
        rooms: Vec<RoomId>,
    },
}

impl Node {
    /// Returns `true` if the [`RoomId`] matches the provided one
    #[must_use]
    pub fn room_id_eq(&self, id: &RoomId) -> bool {
        match self {
            Node::Room(s_id) => s_id == id,
            Node::Space { space, .. } => space == id,
        }
    }

    /// Returns `true` if the [`RoomId`] is in the provided iterator
    pub fn room_id_in(&self, ids: impl IntoIterator<Item = RoomId>) -> bool {
        let mut iter = ids.into_iter();
        match self {
            Node::Room(id) => iter.any(|x| &x == id),
            Node::Space { space, .. } => iter.any(|x| &x == space),
        }
    }

    /// Returns true if the node is public
    pub async fn is_public(&self, turtle: &Turtle) -> bool {
        let id = match self {
            Node::Room(x) => x,
            Node::Space { space, .. } => space,
        };

        turtle.is_room_public(id).await.unwrap_or(false)
    }
}

/// Walks a space and creates the graph
#[instrument(skip(turtle))]
pub async fn walk_space_raw(turtle: Turtle, space: RoomId) -> Result<HashSet<Node>> {
    let results: Arc<RwLock<HashSet<Node>>> = Arc::new(RwLock::new(HashSet::new()));
    let seen: Arc<RwLock<HashSet<RoomId>>> = Arc::new(RwLock::new(HashSet::new()));
    let mut stack: Vec<RoomId> = vec![space];
    while !stack.is_empty() {
        let futures = stack
            .drain(..)
            .map(|room| {
                let seen = seen.clone();
                let results = results.clone();
                let turtle = turtle.clone();
                task::spawn(
                    async move {
                        let mut stack = vec![];
                        if seen.read().await.contains(&room) {
                            trace!(?room, "Room already seen, skipping");
                        } else {
                            trace!("Exploring room: {:?}", room);
                            seen.write().await.insert(room.clone()); // Add room to cycle detection cache
                            let children = list_childern(&turtle, &room).await;
                            match children {
                                Ok(children) => {
                                    if children.is_empty() {
                                        trace!(?room, "Non-space room");
                                        results.write().await.insert(Node::Room(room));
                                    } else {
                                        trace!(?room, "Space detected");
                                        results.write().await.insert(Node::Space {
                                            space: room,
                                            rooms: children.clone(),
                                        });
                                        stack.extend(children);
                                    }
                                }
                                Err(e) => trace!(?e, ?room, "Failed to list room"),
                            }
                        }
                        let x: Result<Vec<RoomId>> = Ok(stack);
                        x
                    }
                    .instrument(info_span!("exploring room")),
                )
            })
            .collect::<Vec<_>>();
        let children: Vec<Vec<RoomId>> = join_all(futures)
            .await
            .into_iter()
            .map(|x| x.wrap_err("Failed to join future"))
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .collect::<Result<Vec<_>>>()?;
        for child in children {
            stack.extend(child);
        }
    }
    let x = Ok(results.read().await.clone());
    x
}

/// Returns this rooms list of children, if it has any
#[instrument(skip(turtle))]
async fn list_childern(turtle: &Turtle, space: &RoomId) -> Result<Vec<RoomId>> {
    trace!(?space, "Requesting state for room");
    let state = turtle.get_state(space).await?;
    trace!(?space, "Room state acquired");
    // Parse the response into a list of Space Children
    let state = state
        // Filter the space children
        .into_iter()
        .filter_map(|m| {
            if let AnyStateEvent::SpaceChild(x) = m {
                if x.content.via.is_some()
                    || x.content.order.is_some()
                    || x.content.suggested.is_some()
                {
                    Some(x)
                } else {
                    None
                }
            } else {
                None
            }
        })
        // Pull out the room ids from the space children
        .map(|x| RoomId::try_from(x.state_key).wrap_err("Failed to parse a state key"))
        .collect::<Result<Vec<_>>>()?;
    trace!(?space, ?state, "Room state events");
    Ok(state)
}

/// Utility to format a node
pub fn format_node<'a>(
    turtle: Turtle,
    node: Node,
    indentation: usize,
    set: &'a HashSet<Node>,
    filter: bool,
    filtered_prefixes: Vec<&'static str>,
) -> BoxFuture<'a, Option<String>> {
    async move {
        if node.is_public(&turtle).await {
            let buffer = if indentation == 0 {
                String::new()
            } else {
                let mut buffer = String::new();
                for _ in 0..(indentation - 1) {
                    buffer.push_str("    ");
                }
                buffer
            };

            match node {
                Node::Room(id) => {
                    let display_name = turtle.get_room_display_name(&id).await;
                    let alias = if let Ok(alias) = turtle.canonical_alias(id.clone()).await {
                        alias.to_string()
                    } else {
                        id.to_string()
                    };
                    let topic = room_topic(&id, &turtle, filter).await;

                    Some(format!(
                        "{}* [{}](https://matrix.to/#/{}){}\n",
                        buffer, display_name, alias, topic
                    ))
                }
                Node::Space { space, rooms } => {
                    let display_name = turtle.get_room_display_name(&space).await;
                    let link_id = if let Ok(alias) = turtle.canonical_alias(space.clone()).await {
                        alias.to_string()
                    } else {
                        space.to_string()
                    };
                    let mut result = if indentation == 0 {
                        let topic = space_topic(&space, &turtle, indentation, vec![]).await;
                        format!(
                            "# [{}](https://matrix.to/#/{})\n{}",
                            display_name, link_id, topic
                        )
                    } else {
                        let topic =
                            space_topic(&space, &turtle, indentation, filtered_prefixes.clone())
                                .await;
                        format!(
                            "{}* [{}](https://matrix.to/#/{})\n{}",
                            buffer, display_name, link_id, topic
                        )
                    };

                    let mut children = set
                        .iter()
                        .filter(|x| x.room_id_in(rooms.iter().cloned()))
                        .collect::<Vec<_>>();
                    children.sort();
                    for child in children {
                        if let Some(output) = format_node(
                            turtle.clone(),
                            child.clone(),
                            indentation + 1,
                            set,
                            filter,
                            filtered_prefixes.clone(),
                        )
                        .await
                        {
                            result.push_str(&output);
                        }
                    }
                    Some(result)
                }
            }
        } else {
            None
        }
    }
    .boxed()
}

/// Helper for filtering topics
fn filter_topic(topic: String, filter: bool) -> String {
    if filter {
        let segments = topic.split('|').collect::<Vec<_>>();
        match segments.len() {
            2 => segments[0].to_string(),
            3 => segments[1].to_string(),
            _ => topic,
        }
    } else {
        topic
    }
}

/// Helper for formatting topics
async fn room_topic(room: &RoomId, turtle: &Turtle, filter: bool) -> String {
    if let Some(topic) = turtle
        .get_room_topic(room)
        .await
        .map(|x| filter_topic(x, filter))
    {
        format!(": {}", topic.trim())
    } else {
        String::new()
    }
}

/// Helper for formatting topics
async fn space_topic(
    room: &RoomId,
    turtle: &Turtle,
    indentation: usize,
    filtered_prefixes: Vec<&str>,
) -> String {
    if let Some(topic) = turtle.get_room_topic(room).await {
        let buffer = if indentation == 0 {
            String::new()
        } else {
            let mut buffer = "  ".to_string();
            for _ in 0..indentation - 1 {
                buffer.push_str("    ");
            }
            buffer
        };
        let lines: String = topic
            .split('\n')
            .filter(|x| !x.trim().is_empty())
            .filter(|x| {
                for prefix in &filtered_prefixes {
                    if x.starts_with(prefix) {
                        return false;
                    }
                }
                true
            })
            .map(|x| format!("{}{}\n\n", buffer, x))
            .collect::<Vec<_>>()
            .iter()
            .flat_map(|x| x.chars())
            .collect();
        format!("\n{}\n", lines)
    } else {
        String::new()
    }
}
