#![warn(
    clippy::all,
    clippy::pedantic,
    rust_2018_idioms,
    missing_docs,
    clippy::missing_docs_in_private_items
)]
#![allow(
    clippy::option_if_let_else,
    clippy::module_name_repetitions,
    clippy::shadow_unrelated,
    clippy::must_use_candidate,
    clippy::implicit_hasher
)]

//! Work in progress, featureful, matrix bot framework
pub mod config;
/// Data structures used by bot
pub mod data;
/// Space walker v2
pub mod space_directory;
pub mod space_walker;

use std::sync::Arc;

use eyre::{eyre, ContextCompat, Result, WrapErr};
use matrix_sdk::{
    room::Room,
    ruma::{
        api::{
            self,
            client::{
                error::ErrorKind,
                r0::{alias::get_alias, profile::get_display_name, state::get_state_events},
            },
            error::{FromHttpResponseError, ServerError},
        },
        events::{
            room::{
                join_rules::JoinRule,
                member::MemberEventContent,
                message::{MessageEventContent, MessageType},
            },
            AnyStateEvent, StrippedStateEvent, SyncMessageEvent,
        },
        RoomAliasId, RoomId, UserId,
    },
    Client, ClientConfig, HttpError, SyncSettings,
};
use space_directory::SpaceDirectory;
use tokio::{
    sync::{Mutex, RwLock},
    time::{sleep, Duration},
};
use tracing::{debug, error, info, info_span, instrument, trace, warn, Instrument};

use crate::{
    config::ConfigFile,
    data::{
        command::{Command, CommandParser},
        logger::Logger,
        lru_time_cache::Cache,
        timer::Timer,
    },
};

/// Core bot structure
#[derive(Clone)]
pub struct Turtle {
    /// Configuration for the bot
    pub config_file: Arc<RwLock<ConfigFile>>,
    /// Client instance
    pub client: Client,
    /// Room State Cache
    room_state_cache: Arc<Mutex<Cache<RoomId, Vec<AnyStateEvent>, 1024>>>,
    /// [`CommandParser`] for the bot
    command_parser: Arc<RwLock<CommandParser>>,
    /// Space Directory
    space_directory: Arc<RwLock<SpaceDirectory>>,
    /// multi-logger for the bot
    logger: Arc<Mutex<Logger>>,
}

impl Turtle {
    /// Creates a bot instance and connects the client
    #[instrument(skip(config_file, prefixes), err)]
    pub async fn new(
        mut config_file: ConfigFile,
        prefixes: impl IntoIterator<Item = impl AsRef<str>>,
    ) -> Result<Self> {
        let client_config =
            ClientConfig::new().store_path(&config_file.config.directories.store_dir);
        let client =
            Client::new_with_config(config_file.config.login.homeserver.clone(), client_config)
                .wrap_err("Failed to instantiate client")?;
        // Login if this is the first time, restore otherwise
        if let Some(session) = config_file.config.login.try_session() {
            info!("Restoring existing session");
            client
                .restore_login(session)
                .await
                .wrap_err("Failed to restore session")?;
        } else {
            info!("Logging in for the first time");
            // Get password
            let pass = rpassword::read_password_from_tty(Some("Enter bot's password:"))
                .wrap_err("failed to read password")?;
            let response = client
                .login(
                    config_file.config.login.username.as_str(),
                    &pass,
                    None,
                    Some(&config_file.config.login.display_name),
                )
                .await
                .wrap_err("Failed to perform initial login")?;
            // Update config file with device id and access token
            let device_id = client
                .device_id()
                .await
                .wrap_err("Client somehow does not have a device id")?;
            let access_token = response.access_token;
            config_file.config.login.device_id = Some(device_id);
            config_file.config.login.access_token = Some(access_token);
            config_file
                .update_file()
                .wrap_err("Failed to write device id")?;
            debug!("Updated config file with device id");
        }
        // Create the command processor
        let command_parser = CommandParser::new(prefixes);
        // Create the space directory
        let space_directory = Arc::new(RwLock::new(SpaceDirectory::new(
            client.clone(),
            config_file
                .config
                .login
                .access_token
                .clone()
                .wrap_err("No access token!")?,
        )));
        let display_name = config_file.config.login.display_name.clone();
        // Set display name
        let set_display_name = match client.display_name().await {
            Ok(Some(x)) => x != display_name,
            _ => true,
        };
        if set_display_name {
            info!(?display_name, "Setting display name");
            client
                .set_display_name(Some(&display_name))
                .await
                .wrap_err("Failed to set display name")?;
        }

        let logger = Logger::new(client.clone(), config_file.config.log_channel.clone());

        logger.log_message("Bot started").await?;

        Ok(Turtle {
            config_file: Arc::new(RwLock::new(config_file)),
            client,
            room_state_cache: Arc::new(Mutex::new(Cache::new(Duration::from_secs(3600)))),
            command_parser: Arc::new(RwLock::new(command_parser)),
            space_directory,
            logger: Arc::new(Mutex::new(logger)),
        })
    }

    /// Registers a new command into the turtle
    pub async fn register_command(&self, command: impl Command + 'static) {
        self.command_parser
            .write()
            .await
            .commands
            .push(Box::new(command));
    }

    /// Registers the event handlers
    ///
    /// # Errors
    ///
    /// Will return an error if there is an underlying `matrix_sdk` error
    #[instrument(skip(self))]
    pub async fn register_handlers(&self) -> Result<()> {
        // AutoJoin handler
        let turt = self.clone();
        self.client
            .register_event_handler(move |room_member, room| {
                let turt = turt.clone();
                async move { turt.auto_join(room_member, room).await }
            })
            .await;
        info!("Registered autojoin handler");
        // Message handler
        let turt = self.clone();
        self.client
            .register_event_handler(move |event, room| {
                let turt = turt.clone();
                async move { turt.handle_message(event, room).await }
            })
            .await;
        info!("Registered message handler");
        Ok(())
    }

    /// Register a timer
    ///
    /// This function will be called repeatedly, at the interval specified by the duration argument, with
    /// the turtle instance provided as an argument.
    pub async fn register_timer<T>(&self, duration: Option<Duration>, mut timer: T)
    where
        T: Timer,
    {
        let turt = self.clone();
        let name = timer.name();
        tokio::spawn(
            async move {
                // Create interval
                let mut interval =
                    tokio::time::interval(duration.unwrap_or_else(|| Duration::from_secs(600)));
                interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Delay);
                // Enter timer loop
                loop {
                    interval.tick().await;
                    if let Err(e) = timer.call(&turt).await {
                        warn!(?e, "Error calling timer");
                    }
                }
            }
            .instrument(info_span!("Background timer", ?name)),
        );
    }

    /// Setup cross signing, if needed, and sync client once
    #[instrument(skip(self))]
    pub async fn inital_sync(&self) -> Result<()> {
        info!(
            "Cross signing: {:?}",
            self.client.cross_signing_status().await
        );
        if !self.config_file.read().await.config.initial_sync {
            use matrix_sdk::ruma::{api::client::r0::uiaa, assign};
            info!("Bootstrapping cross signing");
            let cross_singing_pass =
                rpassword::read_password_from_tty(Some("Enter cross singing password:"))
                    .wrap_err("Failed to read cross pass")?;
            let username = self
                .config_file
                .read()
                .await
                .config
                .login
                .username
                .as_str()
                .to_string();
            if let Err(e) = self.client.bootstrap_cross_signing(None).await {
                trace!(?e, "Got failed cross signing response, expected");
                if let Some(response) = e.uiaa_response() {
                    let auth_data = uiaa::AuthData::Password(assign!(uiaa::Password::new(
                        uiaa::UserIdentifier::MatrixId( &username ),
                            &cross_singing_pass
                        ),
                        { session: response.session.as_deref() }
                    ));
                    self.client
                        .bootstrap_cross_signing(Some(auth_data))
                        .await
                        .wrap_err("Failed to bootstrap cross signing")?;
                } else {
                    return Err(eyre!("Error during cross signing bootstrap"));
                }
            } else {
                return Err(eyre!("Error during cross signing bootstrap"));
            }

            info!("Doing initial sync");
            let sync_settings = SyncSettings::new().full_state(true);
            self.client
                .sync_once(sync_settings)
                .await
                .wrap_err("Failed to sync")?;
            let mut cfg = self.config_file.write().await;
            cfg.config.initial_sync = true;
            cfg.update_file()
                .wrap_err("Failed to set initial sync flag in config")?;
        }
        Ok(())
    }

    /// Enter sync loop
    #[instrument(skip(self))]
    pub async fn main_loop(&self) {
        let sync_settings = if let Some(sync_token) = self.client.sync_token().await {
            SyncSettings::new().token(sync_token).full_state(true)
        } else {
            warn!("Proceeding without sync token");
            SyncSettings::new()
        };
        self.client.sync(sync_settings).await;
    }

    /// Handle auto-joins
    #[instrument(skip(self))]
    async fn auto_join(&self, room_member: StrippedStateEvent<MemberEventContent>, room: Room) {
        if room_member.state_key != self.client.user_id().await.unwrap() {
            return;
        }
        if let Room::Invited(room) = room {
            debug!(?room_member, ?room, "Processing invite");
            let config_file = self.config_file.read().await;
            let config = &config_file.config;
            // Check our criteria
            // First, see if the invite was a direct message
            let join_room = if config.auto_join.direct_message
                && room_member.content.is_direct.unwrap_or(false)
            {
                info!("Joining direct message");
                true
            } else if config.auto_join.invite_from_admin
                && config.admins.contains(&room_member.sender)
            {
                info!("Joining from admin invite");
                true
            } else {
                false
            };

            let mut delay = 500;
            if join_room {
                info!("Joining room: {:?}", room_member);
                while let Err(err) = room.accept_invitation().await {
                    warn!(?room_member, ?err, "Temporary issue joining room");
                    sleep(Duration::from_millis(delay)).await;
                    delay *= 2;
                    if delay > 1000 * 300 {
                        error!(?room_member, ?err, "Failed to join room");
                        break;
                    }
                }
                info!("Joined room!");
            } else {
                warn!(?room_member, "Rejecting invite");
                while let Err(err) = room.reject_invitation().await {
                    warn!(?room_member, ?err, "Temporary issue rejecting room");
                    sleep(Duration::from_millis(delay)).await;
                    delay *= 2;
                    if delay > 1000 * 300 {
                        error!(?room_member, ?err, "Failed to rejecting room");
                        break;
                    }
                }
            }
        }
    }

    /// Handle messages
    #[instrument(skip(self, message, room))]
    async fn handle_message(&self, message: SyncMessageEvent<MessageEventContent>, room: Room) {
        if !room.are_members_synced() {
            trace!("Syncing members");
            match room.sync_members().await {
                Ok(_) => trace!("Members synced"),
                Err(e) => error!(?e, "Failed to sync members!"),
            }
        }
        let direct_message = match self.is_direct(room.room_id()).await {
            Ok(v) => v,
            Err(error) => {
                error!(
                    ?error,
                    "Failed to determine if room is a direct message or not."
                );
                false
            }
        };
        let command_parser = self.command_parser.read().await;
        match command_parser
            .parse_command(message.clone(), &room, direct_message)
            .await
        {
            Some(rc) => {
                debug!("Parsed message as a command");
                match command_parser.execute_command(rc, self).await {
                    Some(Ok(())) => trace!("Command processed"),
                    Some(Err(e)) => {
                        error!(?e, "Error while processing command");
                        let content = if let MessageType::Text(text) = message.content.msgtype {
                            text.body
                        } else {
                            " ".to_string()
                        };
                        let log_message = format!(
                            "Command `{}` issued by {} in {} failed!\n```\n{:#?}\n```\n",
                            content,
                            message.sender,
                            room.room_id(),
                            e
                        );
                        self.log_message(log_message).await.expect("Failed to log");
                    }
                    None => trace!("No matching commands"),
                }
            }
            None => trace!("Failed to parse message as a command, ignoring"),
        }
    }

    /// Gets the canonical alias for a room
    #[instrument(skip(self))]
    pub async fn canonical_alias(&self, id: RoomId) -> Result<RoomAliasId> {
        // Poll the room state to get the alias
        trace!(?id, "Pooling state to find alias of room");
        let state = self.get_state(&id).await?;
        trace!(?id, "Room state acquired");
        // Parse the response to find the canonical_alias
        let state = state
            // Filter the room alias
            .into_iter()
            .filter_map(|m| {
                if let AnyStateEvent::RoomCanonicalAlias(x) = m {
                    Some(x)
                } else {
                    None
                }
            })
            // Pull out the alias
            .filter_map(|x| x.content.alias)
            .collect::<Vec<_>>();
        if state.len() == 1 {
            Ok(state[0].clone())
        } else {
            Err(eyre!("Room has an invalid alias count"))
        }
    }

    /// Returns the state of a room, directly reaching out over the network to get it
    #[instrument(skip(self), err)]
    async fn get_state_raw(&self, id: &RoomId) -> Result<Vec<AnyStateEvent>> {
        trace!(?id, "Requesting state for room");
        let request = get_state_events::Request::new(id);
        let response = match self.client.send(request, None).await {
            Ok(x) => x,
            Err(e) => {
                error!(?e, "Failed to get room state");
                return Err(e.into());
            }
        };
        trace!("Room state acquired");
        response
            .room_state
            .into_iter()
            .map(|x| x.deserialize().wrap_err("Failed to deser response"))
            .collect::<Result<Vec<_>>>()
    }

    /// Queries the network for the state of the room, updating the cache before returning the
    /// result
    #[instrument(skip(self))]
    pub async fn get_state_direct(&self, id: &RoomId) -> Result<Vec<AnyStateEvent>> {
        // Query for result
        let result = self.get_state_raw(id).await?;
        trace!("Raw State gotten");
        // Insert into cache
        self.room_state_cache
            .lock()
            .await
            .insert(id.clone(), result.clone());
        trace!("Cache updated");
        Ok(result)
    }

    /// Returns the state of a room, using the cache if available, updating the cache if not
    /// available
    #[instrument(skip(self))]
    pub async fn get_state(&self, id: &RoomId) -> Result<Vec<AnyStateEvent>> {
        let cache_result = {
            let mut cache = self.room_state_cache.lock().await;
            trace!("Cache locked");
            let res = cache.get(id).cloned();
            std::mem::drop(cache);
            trace!("Cache unlocked");
            res
        };
        // Check the cache
        if let Some(cache_result) = cache_result {
            trace!(?id, "Found in cache");
            Ok(cache_result)
        } else {
            trace!(?id, "Reaching out to network");
            self.get_state_direct(id).await
        }
    }

    /// Determines if a room is a direct message or not, using the cache
    ///
    /// Will first attempt to use the `is_direct` flag, if it is available from the current room state. If
    /// that flag is not available, it will result to the fallback strategy of checking if the room has
    /// exactly two members and is invitation only.
    ///
    /// # Errors
    ///
    /// Will bubble up any underlying network errors that occur if network accesses is needed (and fails)
    /// due to the room not being in the state cache
    #[instrument(skip(self))]
    pub async fn is_direct(&self, id: &RoomId) -> Result<bool> {
        // Attempt to grab the is_direct tag from the room state
        for event in self.get_state(id).await? {
            if let AnyStateEvent::RoomMember(event) = event {
                trace!(?event, "Room state event");
                if let Some(val) = event.content.is_direct {
                    return Ok(val);
                }
                if let Some(prev) = event.prev_content {
                    if let Some(val) = prev.is_direct {
                        return Ok(val);
                    }
                }
            }
        }
        // Fallback detection
        // First check that the room is invite only
        let join_rules = self
            .get_state(id)
            .await?
            .into_iter()
            .filter_map(|x| {
                if let AnyStateEvent::RoomJoinRules(x) = x {
                    Some(x)
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();
        if join_rules.len() == 1 && join_rules[0].content.join_rule == JoinRule::Invite {
            // Now check the number of members
            let members = self
                .get_state(id)
                .await?
                .into_iter()
                .filter_map(|x| {
                    if let AnyStateEvent::RoomMember(x) = x {
                        Some(x)
                    } else {
                        None
                    }
                })
                .count();
            if members == 2 {
                return Ok(true);
            }
        }
        Ok(false)
    }

    /// Determines a user's room specific display name
    ///
    /// Fallback to the users overall profile name, if a room specific one is not found.
    ///
    /// # Errors
    ///
    /// Will bubble up any underlying network errors that occur if network access is needed (and fails) due
    /// to either the room or the user not being in the state cache.
    #[instrument(skip(self), err)]
    pub async fn get_display_name(&self, room: Option<&RoomId>, user: &UserId) -> Result<String> {
        // See if we can find the user's display name in the room state
        let display_name_maybe = if let Some(room) = room {
            self.get_state(room)
                .await?
                .into_iter()
                .filter_map(|x| {
                    if let AnyStateEvent::RoomMember(x) = x {
                        Some(x)
                    } else {
                        None
                    }
                })
                .filter(|x| &x.sender == user)
                .find_map(|x| x.content.displayname)
        } else {
            None
        };
        if let Some(display_name) = display_name_maybe {
            Ok(display_name)
        } else {
            // Fallback detection, we'll build the request manually
            let request = get_display_name::Request::new(user);
            let response = self
                .client
                .send(request, None)
                .await
                .context("Failed to request user's display name from homeserver")?;
            if let Some(displayname) = response.displayname {
                Ok(displayname)
            } else {
                Err(eyre!("User {} does not have a display name set", user))
            }
        }
    }

    /// Determines the [`RoomId`] of a room given a [`RoomAliasId`]
    ///
    /// Will return `None` if the room could not be found, or if the bot does not have the permissions
    /// required to see it.
    ///
    /// # Errors
    ///
    /// This method reaches out to the homeserver over the network, and will bubble up any errors that are
    /// encountered.
    #[instrument(skip(self), err)]
    pub async fn get_room_id(&self, alias: &RoomAliasId) -> Result<Option<RoomId>> {
        let request = get_alias::Request::new(alias);
        let response = self.client.send(request, None).await;
        match response {
            Ok(x) => Ok(Some(x.room_id)),
            Err(HttpError::ClientApi(FromHttpResponseError::Http(ServerError::Known(
                api::client::Error {
                    kind: ErrorKind::NotFound,
                    ..
                },
            )))) => Ok(None),
            Err(e) => Err(e.into()),
        }
    }

    /// Returns the list of spaces that the turtle is in, as [`RoomId`]s.
    ///
    /// # Errors
    ///
    /// Will return an error if any of the [`RoomAliasId`]s in the configuration are malformed or
    /// non-existent.
    #[instrument(skip(self), err)]
    pub async fn get_spaces(&self) -> Result<Vec<RoomId>> {
        let spaces = self.config_file.read().await.config.spaces.clone();
        let mut output = vec![];
        for space in spaces {
            let id = self
                .get_room_id(&space)
                .await
                .wrap_err("Failure in network access getting spaces")?
                .wrap_err("Space did not exist")?;
            output.push(id);
        }
        Ok(output)
    }

    /// Returns the list of administrators for this turtle from the config
    pub async fn get_admins(&self) -> Vec<UserId> {
        self.config_file.read().await.config.admins.clone()
    }

    /// Returns true if the room is public
    ///
    /// # Errors
    ///
    /// This method may reach out to the network to get room state, and will bubble up any errors that occur.
    #[instrument(skip(self), err)]
    pub async fn is_room_public(&self, room: &RoomId) -> Result<bool> {
        let result = self.get_state(room).await;
        match result {
            Ok(state) => {
                // Find the joinrule
                let join_rule = state.into_iter().find_map(|x| {
                    if let AnyStateEvent::RoomJoinRules(x) = x {
                        Some(x)
                    } else {
                        None
                    }
                });
                if let Some(join_rule) = join_rule {
                    Ok(join_rule.content.join_rule == JoinRule::Public)
                } else {
                    warn!("Room doesn't have a join_rule");
                    Ok(false)
                }
            }
            // TODO: explicitly distinguish ErrorKind::Forbidden from other errors
            Err(_) => Ok(false),
        }
    }

    /// Returns the name of the room, with fallback
    ///
    /// The fallback order is:
    ///    1. The room's name (current value of `m.room.name`)
    ///    2. The room's canonical alias
    ///    3. The room's id
    #[instrument(skip(self))]
    pub async fn get_room_display_name(&self, room: &RoomId) -> String {
        let state = self.get_state(room).await;
        if let Ok(state) = state {
            let name = state
                .into_iter()
                .find_map(|x| {
                    if let AnyStateEvent::RoomName(x) = x {
                        Some(x)
                    } else {
                        None
                    }
                })
                .and_then(|x| x.content.name)
                .map(|x| x.to_string());
            if let Some(name) = name {
                name
            } else {
                let alias = self
                    .canonical_alias(room.clone())
                    .await
                    .map_or_else(|_| room.to_string(), |x| x.to_string());
                alias
            }
        } else {
            room.to_string()
        }
    }

    /// Returns the topic for a room, if there is one
    #[instrument(skip(self))]
    pub async fn get_room_topic(&self, room: &RoomId) -> Option<String> {
        if let Ok(state) = self.get_state(room).await {
            state
                .into_iter()
                .find_map(|x| {
                    if let AnyStateEvent::RoomTopic(x) = x {
                        Some(x)
                    } else {
                        None
                    }
                })
                .map(|x| x.content.topic)
        } else {
            None
        }
    }

    /// Gets the directory listing for a particular space.
    ///
    /// # Errors
    ///
    /// Will error out if:
    ///   * Required network access fails
    ///   * The space does not exist
    ///   * any json fails to parse
    #[instrument(skip(self))]
    pub async fn get_hierarchy(&self, room: &RoomId) -> Result<crate::space_directory::Node> {
        let mut sd = self.space_directory.write().await;
        sd.parse(room).await
    }

    /// Logs the message to all available outputs
    ///
    /// # Errors
    ///
    /// Will pass through the underlying error if writing to any of the rooms, files, etc. fails.
    pub async fn log_message(&self, message: impl AsRef<str>) -> Result<()> {
        let logger = self.logger.lock().await;
        logger.log_message(message).await
    }
}
