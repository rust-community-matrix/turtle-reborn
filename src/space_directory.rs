use std::{collections::HashSet, time::Duration};

use matrix_sdk::{
    reqwest,
    ruma::{RoomAliasId, RoomId},
    Client,
};

use eyre::{eyre, Result, WrapErr};
use futures::{future::BoxFuture, FutureExt};
use serde::{Deserialize, Serialize};
use tracing::{info, instrument, trace, warn};

use crate::data::lru_time_cache::Cache;

/// Struct (with a cache) for walking spaces
pub struct SpaceDirectory {
    /// Underlying client, used to get the homeserver url
    client: Client,
    /// Access token, since these are authenticated requests
    access_token: String,
    /// LRU + time cache
    directory_cache: Cache<RoomId, Vec<DirectoryEntry>, 1024>,
}

impl SpaceDirectory {
    /// Creates a new `SpaceDirectory`, given a client and an access token
    pub fn new(client: Client, access_token: String) -> Self {
        Self {
            client,
            directory_cache: Cache::new(Duration::from_secs(3600)),
            access_token,
        }
    }

    #[instrument(skip(self))]
    async fn lookup_raw(&self, id: &RoomId) -> Result<Vec<DirectoryEntry>> {
        let request = format!(
            "{}_matrix/client/unstable/org.matrix.msc2946/rooms/{}/hierarchy?access_token={}",
            self.client.homeserver().await,
            id,
            self.access_token
        );
        info!(?request);
        let result = reqwest::get(request)
            .await
            .wrap_err("Space directory network request failed")?
            .text()
            .await
            .wrap_err("Response did not have text")?;
        let directory: Directory =
            serde_json::from_str(&result).wrap_err("Failed to deserialize")?;
        Ok(directory.rooms)
    }

    /// Performs the raw network access for the hierarchy endpoint for the specified [`RoomId`], and parses
    /// the returned JSON into a list of [`DirectoryEntry`]s. The results are cached.
    ///
    /// # Errors
    ///
    /// Will return an error if the network access fails, or the JSON fails to deserialize.
    #[instrument(skip(self))]
    pub async fn lookup_raw_cached(&mut self, id: &RoomId) -> Result<Vec<DirectoryEntry>> {
        let cache_result = self.directory_cache.get(id).cloned();
        // Check the cache
        if let Some(cache_result) = cache_result {
            trace!(?id, "Found in cache");
            Ok(cache_result)
        } else {
            trace!(?id, "Reaching out to network");
            let result = self.lookup_raw(id).await?;
            self.directory_cache.insert(id.clone(), result.clone());
            trace!("cache updated");
            Ok(result)
        }
    }

    /// Gets the directory entries for a [`RoomId`] from the hierarchy endpoint, and parses them into a
    /// [`Node`] data structure.
    ///
    ///
    /// # Errors
    ///
    /// Will return an error if the network access fails, or if the JSON fails to deserialize.
    #[instrument(skip(self))]
    pub async fn parse<'a, 'b>(&'a mut self, id: &'b RoomId) -> Result<Node>
    where
        'b: 'a,
    {
        let mut seen = HashSet::new();
        self.parse_inner(id, &mut seen, None).await
    }

    #[instrument(skip(self))]
    fn parse_inner<'a, 'b, 'c>(
        &'a mut self,
        id: &'b RoomId,
        seen: &'c mut HashSet<RoomId>,
        prev_result: Option<Vec<DirectoryEntry>>,
    ) -> BoxFuture<'a, Result<Node>>
    where
        'b: 'a,
        'c: 'a,
    {
        async move {
            let (result, space) = if let Some(result) = prev_result {
                if let Some(space) = result.iter().find(|x| &x.room_id == id).cloned() {
                    (result, space)
                } else {
                    let result = self.lookup_raw_cached(id).await?;
                    let space = result[0].clone();
                    (result, space)
                }
            } else {
                let result = self.lookup_raw_cached(id).await?;
                let space = result[0].clone();
                (result, space)
            };
            let rest = &result[..];
            if seen.contains(&space.room_id) {
                Err(eyre!("Revisiting room {}", id))
            } else {
                seen.insert(space.room_id.clone());
                let children = &space.children_state;
                let common = Common {
                    room_id: space.room_id.clone(),
                    name: space.name.clone(),
                    topic: space.topic.clone(),
                    alias: space.canonical_alias.clone(),
                    public: space.join_rules == "public",
                };
                if space.room_type.as_deref() == Some("m.space") {
                    let mut parsed_children = vec![];
                    for child in children {
                        if let Some(c) = rest.iter().find(|x| x.room_id == child.state_key) {
                            if let Some(x) = c.to_room() {
                                parsed_children.push(x);
                            } else {
                                match self
                                    .parse_inner(&child.state_key, seen, Some(result.clone()))
                                    .await
                                {
                                    Ok(c) => parsed_children.push(c),
                                    Err(e) => warn!(?e, "error parsing child"),
                                }
                            }
                        } else {
                            match self
                                .parse_inner(&child.state_key, seen, Some(result.clone()))
                                .await
                            {
                                Ok(c) => parsed_children.push(c),
                                Err(e) => warn!(?e, "error parsing child"),
                            }
                        }
                    }
                    Ok(Node::Space {
                        common,
                        children: parsed_children,
                    })
                } else {
                    Ok(Node::Room { common })
                }
            }
        }
        .boxed()
    }
}

/// Parsing data structure for the response from the hierarchy endpoint
#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Directory {
    /// The `rooms` field of the response
    pub rooms: Vec<DirectoryEntry>,
}
/// Parsing data structure for the response from the hierarchy endpoint
#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct DirectoryEntry {
    /// The `room_id` field of the response
    pub room_id: RoomId,
    /// The `name` field of the response
    pub name: Option<String>,
    /// The `topic` field of the response
    pub topic: Option<String>,
    /// the `canonical_alias` field of the response
    pub canonical_alias: Option<RoomAliasId>,
    /// the `num_joined_members` field of the response
    pub num_joined_members: u64,
    /// the `join_rules` field of the response
    pub join_rules: String,
    /// the `room_type` field of the response
    pub room_type: Option<String>,
    /// The entry's children
    pub children_state: Vec<ChildEntry>,
}

impl DirectoryEntry {
    /// Helper function for building the directory from a response
    fn to_room(&self) -> Option<Node> {
        if self.room_type.is_none() {
            Some(Node::Room {
                common: Common {
                    room_id: self.room_id.clone(),
                    name: self.name.clone(),
                    topic: self.topic.clone(),
                    alias: self.canonical_alias.clone(),
                    public: self.join_rules == "public",
                },
            })
        } else {
            None
        }
    }
}

/// Parsing data structure for the response from the hierarchy endpoint
#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct ChildEntry {
    /// Child
    pub state_key: RoomId,
    /// Parent
    pub room_id: RoomId,
}

/// Common data elements, shared between
#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Common {
    /// The raw [`RoomId`] of this node
    pub room_id: RoomId,
    /// The room's display name, if it has one
    pub name: Option<String>,
    /// The topic of the room, if it has one
    pub topic: Option<String>,
    /// The alias of the room, if it has one
    pub alias: Option<RoomAliasId>,
    /// Whether or not this room is public
    pub public: bool,
}

/// Either a space or a room
#[derive(Deserialize, Serialize, Clone, Debug)]
pub enum Node {
    /// A space, having common state and child rooms
    Space {
        /// Common attributes
        common: Common,
        /// The child rooms of this space
        children: Vec<Node>,
    },
    /// A room, with no children
    Room {
        /// Common attributes
        common: Common,
    },
}

impl Node {
    /// Gets a reference to the common attributes portion of this `Node`
    pub fn common(&self) -> &Common {
        match self {
            Node::Space { common, .. } | Node::Room { common } => common,
        }
    }

    /// Gets a reference to the children of this `Node`, if it has any
    pub fn children(&self) -> Option<&[Node]> {
        match self {
            Node::Space { children, .. } => Some(children.as_slice()),
            Node::Room { .. } => None,
        }
    }

    /// Returns true if this `Node` is a space
    pub fn is_space(&self) -> bool {
        match self {
            Node::Space { .. } => true,
            Node::Room { .. } => false,
        }
    }
}

impl Node {
    /// Formats a node as a basic textual list
    pub fn basic_format(&self) -> String {
        self.basic_format_inner(1)
    }

    /// Interior helper function for `basic_format`, to keep track of indentation
    fn basic_format_inner(&self, indentation: usize) -> String {
        let mut buffer = String::new();
        if indentation > 0 {
            for _ in 0..indentation - 1 {
                buffer.push_str("  ");
            }
        }
        let common = self.common();
        let display_name = common
            .name
            .as_deref()
            .or_else(|| common.alias.as_ref().map(|x| -> &str { x.as_str() }))
            .unwrap_or_else(|| common.room_id.as_str());
        let alias = common
            .alias
            .as_ref()
            .map_or_else(|| common.room_id.as_str(), RoomAliasId::as_str);
        let mut output = format!(
            "{}- {}: {} ({})\n",
            buffer, display_name, alias, common.room_id
        );
        match self {
            Node::Space { children, .. } => {
                // Put non-space children first
                for child in children.iter().filter(|x| !x.is_space()) {
                    let fmt_child = child.basic_format_inner(indentation + 1);
                    output.push_str(&fmt_child);
                }
                for child in children.iter().filter(|x| x.is_space()) {
                    let fmt_child = child.basic_format_inner(indentation + 1);
                    output.push_str(&fmt_child);
                }
                output
            }
            Node::Room { .. } => output,
        }
    }

    /// Formats a node as a markdown list
    pub fn markdown_format(&self) -> String {
        self.markdown_format_inner(0)
    }

    /// Interior helper function for `markdown_format`, to keep track of indentation
    fn markdown_format_inner(&self, indentation: usize) -> String {
        let mut buffer = String::new();
        if indentation > 0 {
            for _ in 0..(indentation - 1) {
                buffer.push_str("    ");
            }
        }
        let common = self.common();
        let display_name = common
            .name
            .as_deref()
            .or_else(|| common.alias.as_ref().map(|x| -> &str { x.as_str() }))
            .unwrap_or_else(|| common.room_id.as_str());
        let alias = common
            .alias
            .as_ref()
            .map_or_else(|| common.room_id.as_str(), RoomAliasId::as_str);
        let self_link = format!("[{}](https://matrix.to/#/{})", display_name, alias);
        let mut output = if indentation == 0 && self.is_space() {
            let topic = self.format_main_topic();
            format!("# {}{}\n", self_link, topic)
        } else {
            let topic = if indentation > 0 {
                self.format_topic(indentation)
            } else {
                self.format_topic(1)
            };
            format!("{}* {}{}\n", buffer, self_link, topic)
        };
        if let Node::Space { children, .. } = self {
            // Put non-space children first
            for child in children.iter().filter(|x| !x.is_space()) {
                let fmt_child = child.markdown_format_inner(indentation + 1);
                output.push_str(&fmt_child);
            }
            for child in children.iter().filter(|x| x.is_space()) {
                let fmt_child = child.markdown_format_inner(indentation + 1);
                output.push_str(&fmt_child);
            }
        }
        output
    }

    /// Generically format the topic for a node
    fn format_topic(&self, indentation: usize) -> String {
        match self {
            Node::Space { common, .. } => {
                match &common.topic {
                    Some(topic) => {
                        // make the buffer
                        let mut buffer = "  ".to_string();
                        for _ in 0..(indentation - 1) {
                            buffer.push_str("    ");
                        }
                        // Break it into lines, apply indentation, and join
                        let lines_vec = topic
                            .split('\n')
                            .filter(|x| !x.trim().is_empty())
                            .map(|x| format!("{}{}", buffer, x))
                            .collect::<Vec<_>>();
                        if lines_vec.is_empty() {
                            String::new()
                        } else {
                            let lines = lines_vec.join("\n\n");
                            format!("\n\n{}\n", lines)
                        }
                    }
                    None => String::new(),
                }
            }
            Node::Room { common } => match &common.topic {
                // Only include first line of room topics
                Some(x) => {
                    let line = x
                        .split('\n')
                        .next()
                        .map_or_else(String::new, |z| z.trim().to_string());
                    format!(": {}", line)
                }
                None => String::new(),
            },
        }
    }

    /// Format the topic for the root node
    fn format_main_topic(&self) -> String {
        let common = self.common();
        match &common.topic {
            Some(topic) => {
                let x: String = topic
                    .split('\n')
                    .filter(|x| !x.trim().is_empty())
                    .collect::<Vec<_>>()
                    .join("\n\n");
                format!("\n\n{}\n", x)
            }
            None => String::new(),
        }
    }

    /// Filters this node and its descendant by recursively applying a predicate.
    ///
    /// The predicate is supplied with the current depth (with 0 being the root node), and if the predicate
    /// returns `false`, then the node and all its children will be eliminated.
    pub fn filter(&self, mut pred: impl FnMut(&Node, usize) -> bool) -> Option<Node> {
        self.filter_inner(&mut pred, 0)
    }

    /// Interior helper for the `filter` method
    fn filter_inner(
        &self,
        pred: &mut dyn FnMut(&Node, usize) -> bool,
        depth: usize,
    ) -> Option<Node> {
        if pred(self, depth) {
            match self {
                Node::Space { common, children } => {
                    let mut result = vec![];
                    for child in children {
                        if let Some(node) = child.filter_inner(pred, depth + 1) {
                            result.push(node);
                        }
                    }
                    Some(Node::Space {
                        common: common.clone(),
                        children: result,
                    })
                }
                Node::Room { .. } => Some(self.clone()),
            }
        } else {
            None
        }
    }

    /// Modifies each node in the tree with the given closure.
    ///
    /// The closure is given the current depth (with 0 being the root node).
    pub fn map(&self, mut closure: impl FnMut(&Common, usize) -> Common) -> Node {
        self.map_inner(&mut closure, 0)
    }

    /// Inner helper method for `map`
    fn map_inner(&self, closure: &mut dyn FnMut(&Common, usize) -> Common, depth: usize) -> Node {
        match self {
            Node::Space { common, children } => {
                let common = closure(common, depth);
                let children = children
                    .iter()
                    .map(|x| x.map_inner(closure, depth + 1))
                    .collect();
                Node::Space { common, children }
            }
            Node::Room { common } => Node::Room {
                common: closure(common, depth),
            },
        }
    }

    /// Collapses this tree into a [`HashSet`](std::collections::HashSet) of `RoomId`s
    pub fn room_ids(&self) -> HashSet<RoomId> {
        let mut result = HashSet::new();
        self.room_ids_inner(&mut result);
        result
    }

    /// Internal helper function for `room_ids`
    fn room_ids_inner(&self, map: &mut HashSet<RoomId>) {
        let common = self.common();
        // Skip ourself if we are already in the map
        if !map.contains(&common.room_id) {
            map.insert(common.room_id.clone());
            if let Node::Space { children, .. } = self {
                for child in children {
                    child.room_ids_inner(map);
                }
            }
        }
    }
}

/// Method for transforming pipe separated topics.
///
/// Will perform the following transformations:
///   * A | B | C -> B
///   * A | B -> A
pub fn pipe_split(common: &Common, _depth: usize) -> Common {
    let mut common = common.clone();
    if let Some(topic) = common.topic.clone() {
        let segments = topic
            .split('|')
            .map(str::trim)
            .filter(|x| !x.is_empty())
            .collect::<Vec<_>>();
        match segments.len() {
            2 => {
                common.topic = Some(segments[0].to_string());
                common
            }
            3 => {
                common.topic = Some(segments[1].to_string());
                common
            }
            // Since the topic does not match our format, do nothing
            _ => common,
        }
    } else {
        // If there is no topic, we don't need to do anything
        common
    }
}

/// Method for removing lines matching some prefix from topics.
///
/// The `root_node` flag toggles whether or not this filtering should apply to the root node itself.
pub fn exclude_lines<I, S>(prefixes: I, root_node: bool) -> impl Fn(&Common, usize) -> Common
where
    I: IntoIterator<Item = S>,
    S: AsRef<str>,
{
    /// Helper function
    fn starts_with_prefix(prefixes: &[&str], string: &str) -> bool {
        for prefix in prefixes {
            if string.starts_with(prefix) {
                return true;
            }
        }
        false
    }
    let string_prefixes: Vec<String> = prefixes
        .into_iter()
        .map(|x| x.as_ref().to_string())
        .collect();
    move |common, depth| {
        // If operation on the root node is allowed, or we are past the root node
        if root_node || depth > 0 {
            // Filter the node
            if let Some(topic) = &common.topic {
                let prefixes: Vec<&str> = string_prefixes.iter().map(|x| x.as_ref()).collect();
                let lines = topic.split('\n').map(str::trim).filter(|x| !x.is_empty());
                let mut output_string = String::new();
                for line in lines {
                    if !starts_with_prefix(&prefixes, line) {
                        output_string.push_str(line);
                        output_string.push('\n');
                    }
                }
                let mut common = common.clone();
                common.topic = Some(output_string);
                common
            } else {
                // No topic, return as is
                common.clone()
            }
        } else {
            // Do nothing
            common.clone()
        }
    }
}

/// A filtering function to only include nodes that are public
pub fn only_public(node: &Node, _depth: usize) -> bool {
    node.common().public
}
