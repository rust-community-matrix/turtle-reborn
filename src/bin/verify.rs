use std::{
    fs::{create_dir_all, File, OpenOptions},
    io,
};

use directories::ProjectDirs;
use eyre::{eyre, ContextCompat, Result, WrapErr};
use matrix_sdk::{
    self,
    ruma::{events::AnyToDeviceEvent, UserId},
    verification::{SasVerification, Verification},
    Client, LoopCtrl, SyncSettings,
};
use tracing::{debug, error, info, instrument, warn};

use turtle_reborn::config::ConfigFile;
use turtle_reborn::Turtle;

async fn wait_for_confirmation(client: Client, sas: SasVerification) {
    println!("Does the emoji match: {:?}", sas.emoji());

    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("error: unable to read user input");

    match input.trim().to_lowercase().as_ref() {
        "yes" | "true" | "ok" => {
            sas.confirm().await.unwrap();

            if sas.is_done() {
                print_result(&sas);
                print_devices(sas.other_device().user_id(), &client).await;
            }
        }
        _ => sas.cancel().await.unwrap(),
    }
}

fn print_result(sas: &SasVerification) {
    let device = sas.other_device();

    println!(
        "Successfully verified device {} {} {:?}",
        device.user_id(),
        device.device_id(),
        device.local_trust_state()
    );
}

async fn print_devices(user_id: &UserId, client: &Client) {
    println!("Devices of user {}", user_id);

    for device in client.get_user_devices(user_id).await.unwrap().devices() {
        println!(
            "   {:<10} {:<30} {:<}",
            device.device_id(),
            device.display_name().as_deref().unwrap_or_default(),
            device.verified()
        );
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt()
        .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
        .pretty()
        .init();
    // Find and open config file
    let config_file = open_config()?;
    // Open the turtle
    let turtle = Turtle::new(config_file, ["!"])
        .await
        .wrap_err("Failed to init turtle")?;
    // get the client
    let client = turtle.client.clone();
    let client_ref = &client;

    client
        .sync_with_callback(SyncSettings::new(), |response| async move {
            let _ = &response;
            let client = &client_ref;

            for event in response
                .to_device
                .events
                .iter()
                .filter_map(|e| e.deserialize().ok())
            {
                match event {
                    AnyToDeviceEvent::KeyVerificationStart(e) => {
                        if let Some(Verification::SasV1(sas)) = client
                            .get_verification(&e.sender, &e.content.transaction_id)
                            .await
                        {
                            println!(
                                "Starting verification with {} {}",
                                &sas.other_device().user_id(),
                                &sas.other_device().device_id()
                            );
                            print_devices(&e.sender, client).await;
                            sas.accept().await.unwrap();
                        }
                    }

                    AnyToDeviceEvent::KeyVerificationRequest(e) => {
                        if let Some(r) = client
                            .get_verification_request(&e.sender, &e.content.transaction_id)
                            .await
                        {
                            println!("Accepting request");
                            r.accept().await.unwrap();
                        }
                    }

                    AnyToDeviceEvent::KeyVerificationKey(e) => {
                        if let Some(Verification::SasV1(sas)) = client
                            .get_verification(&e.sender, &e.content.transaction_id)
                            .await
                        {
                            tokio::spawn(wait_for_confirmation((*client).clone(), sas));
                        }
                    }

                    AnyToDeviceEvent::KeyVerificationMac(e) => {
                        if let Some(Verification::SasV1(sas)) = client
                            .get_verification(&e.sender, &e.content.transaction_id)
                            .await
                        {
                            if sas.is_done() {
                                print_result(&sas);
                                print_devices(&e.sender, client).await;
                            }
                        }
                    }

                    _ => (),
                }
            }
            LoopCtrl::Continue
        })
        .await;

    Ok(())
}

/// Find and open the config file
///
/// Creates the folder and default configuration if needed, and will error if the `configured` variable
/// has not been edited.
#[instrument]
fn open_config() -> Result<ConfigFile> {
    let config_dir = ProjectDirs::from("rs", "community", "TurtleReborn")
        .wrap_err("Failed to find config dir")?
        .config_dir()
        .to_path_buf();
    info!("Configuration directory is {:?}", config_dir);
    create_dir_all(&config_dir).wrap_err("Failed to create configuration directory")?;
    let config_path = config_dir.join("config.toml");
    info!("Configuration path is {:?}", config_path);
    let file = OpenOptions::new()
        .read(true)
        .write(true)
        .create(false)
        .open(&config_path);
    if let Ok(file) = file {
        let config_file = ConfigFile::read(file)?;
        if config_file.config.configured {
            debug!("Read config file");
            Ok(config_file)
        } else {
            error!("Please edit the config file and set the configured flag!");
            Err(eyre!(
                "Please edit the config file and set the configured flag!"
            ))
        }
    } else {
        warn!("Creating default configuration file");
        let file = File::create(&config_path)?;
        ConfigFile::write_default(file)?;
        Err(eyre!(
            "Please edit the config file and set the configured flag!"
        ))
    }
}
