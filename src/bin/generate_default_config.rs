use structopt::StructOpt;
use turtle_reborn::config::Config;

use std::convert::TryInto;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "generate_default_config",
    about = "Generates the default configuration file"
)]
struct Opt {
    /// First administrator
    #[structopt(short, long)]
    admin: Option<String>,
    /// First space
    #[structopt(short, long)]
    space: Option<String>,
}

fn main() {
    let opt = Opt::from_args();
    let mut config = Config::default();
    if let Some(admin) = opt.admin {
        config.admins = vec![admin.try_into().unwrap()];
    }
    if let Some(space) = opt.space {
        config.spaces = vec![space.try_into().unwrap()];
    }
    config.source_link = "https://gitlab.com/rust-community-matrix/turtle-reborn".to_string();

    let config_output = toml::to_string_pretty(&config).unwrap();
    println!("{}", config_output);
}
