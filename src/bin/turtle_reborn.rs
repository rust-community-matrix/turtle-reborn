use std::fs::{create_dir_all, File, OpenOptions};

use directories::ProjectDirs;
use eyre::{eyre, ContextCompat, Result, WrapErr};
use tracing::{debug, error, info, instrument, warn};

use turtle_reborn::{
    config::ConfigFile,
    data::command::commands::{DisplaySpace, Ping, VersionInfo},
    data::timer::SpaceAutoJoin,
    Turtle,
};

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt()
        .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
        .pretty()
        .init();
    // Find and open config file
    let config_file = open_config()?;
    // Open the turtle
    let turtle = Turtle::new(config_file, ["!"])
        .await
        .wrap_err("Failed to init turtle")?;
    // Do initial sync
    turtle
        .inital_sync()
        .await
        .wrap_err("Failed to do inital sync")?;
    // Install wanted commands
    turtle.register_command(Ping).await;
    turtle
        .register_command(DisplaySpace {
            pipe_splice: true,
            filtered_prefixes: vec!["Read the rules", "Information and faqs"],
        })
        .await;
    turtle.register_command(VersionInfo).await;
    // Install wanted timers
    turtle.register_timer(None, SpaceAutoJoin).await;
    // Register hooks
    turtle.register_handlers().await?;
    info!("Handlers registered");
    // sync forever
    turtle.main_loop().await;

    Ok(())
}

/// Find and open the config file
///
/// Creates the folder and default configuration if needed, and will error if the `configured` variable
/// has not been edited.
#[instrument]
fn open_config() -> Result<ConfigFile> {
    let config_dir = ProjectDirs::from("rs", "community", "TurtleReborn")
        .wrap_err("Failed to find config dir")?
        .config_dir()
        .to_path_buf();
    info!("Configuration directory is {:?}", config_dir);
    create_dir_all(&config_dir).wrap_err("Failed to create configuration directory")?;
    let config_path = config_dir.join("config.toml");
    info!("Configuration path is {:?}", config_path);
    let file = OpenOptions::new()
        .read(true)
        .write(true)
        .create(false)
        .open(&config_path);
    if let Ok(file) = file {
        let config_file = ConfigFile::read(file)?;
        if config_file.config.configured {
            debug!("Read config file");
            Ok(config_file)
        } else {
            error!("Please edit the config file and set the configured flag!");
            Err(eyre!(
                "Please edit the config file and set the configured flag!"
            ))
        }
    } else {
        warn!("Creating default configuration file");
        let file = File::create(&config_path)?;
        ConfigFile::write_default(file)?;
        Err(eyre!(
            "Please edit the config file and set the configured flag!"
        ))
    }
}
