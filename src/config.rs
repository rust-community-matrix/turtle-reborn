//! Configuration values for the bot
#![allow(clippy::struct_excessive_bools)]
use std::{
    convert::TryFrom,
    fs::File,
    io::{Read, Seek, SeekFrom, Write},
    path::PathBuf,
};

use directories::ProjectDirs;
use matrix_sdk::{
    ruma::{DeviceId, RoomAliasId, RoomId, UserId},
    Session,
};
use serde::{Deserialize, Serialize};
use tracing::{info, instrument};
use url::Url;

use eyre::{Result, WrapErr};

/// Main configuration for the bot
#[derive(Clone, Debug, Serialize, Deserialize, Default)]
pub struct Config {
    /// Toggle to true to indicate that the instance has been configured
    pub configured: bool,
    /// Indicates that the initial sync has been done
    pub initial_sync: bool,
    /// Spaces this bot instance is part of
    pub spaces: Vec<RoomAliasId>,
    /// Bot administrators
    pub admins: Vec<UserId>,
    /// Logging channel
    pub log_channel: Option<RoomId>,
    /// The link to the source code for the bot
    pub source_link: String,
    // Place all tables after here
    /// Login details for the bot
    pub login: LoginDetails,
    /// Directories and files used by the bot
    pub directories: Dirs,
    /// Room auto-join behavior
    pub auto_join: AutoJoin,
}

/// Values for the room join behavior
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord)]
pub struct AutoJoin {
    /// Auto join direct messages
    pub direct_message: bool,
    /// Auto join invited rooms in spaces the bot is connected to
    pub invite_in_spaces: bool,
    /// Auto join rooms from outside the spaces
    pub invite_general: bool,
    /// Auto join rooms unconditionally when invited by a bot administrator
    pub invite_from_admin: bool,
}

impl Default for AutoJoin {
    fn default() -> Self {
        Self {
            direct_message: true,
            invite_in_spaces: true,
            invite_general: false,
            invite_from_admin: true,
        }
    }
}

/// Login details for this bot instance
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord)]
pub struct LoginDetails {
    /// The homeserver's url
    pub homeserver: Url,
    /// Username the bot will try to log in as
    pub username: UserId,
    /// The display name this bot will take
    pub display_name: String,
    /// Access token the bot will attempt to use
    pub access_token: Option<String>,
    /// Device ID, used for restoring the session
    pub device_id: Option<Box<DeviceId>>,
}

impl LoginDetails {
    /// Attempts to create a session out of the known login details
    #[must_use]
    pub fn try_session(&self) -> Option<Session> {
        self.device_id.as_ref().and_then(|device_id| {
            self.access_token.as_ref().map(|access_token| Session {
                access_token: access_token.clone(),
                user_id: self.username.clone(),
                device_id: device_id.clone(),
            })
        })
    }
}

impl Default for LoginDetails {
    fn default() -> Self {
        Self {
            homeserver: Url::parse("https://matrix.community.rs").unwrap(),
            username: UserId::try_from("@turtle-reborn:community.rs").unwrap(),
            access_token: None,
            device_id: None,
            display_name: "Turtle Reborn".to_string(),
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord)]
/// Directories and files used by the bot
pub struct Dirs {
    /// Main store directory
    pub store_dir: PathBuf,
}

impl Default for Dirs {
    fn default() -> Self {
        let store_dir = ProjectDirs::from("rs", "community", "TurtleReborn")
            .unwrap()
            .data_dir()
            .to_path_buf();
        Dirs { store_dir }
    }
}

/// Stores a file and its most recent config
#[allow(clippy::module_name_repetitions)]
pub struct ConfigFile {
    /// Underlying file
    file: File,
    /// Current value
    pub config: Config,
}

impl ConfigFile {
    /// Read a config from a given file
    #[instrument]
    pub fn read(mut file: File) -> Result<ConfigFile> {
        // Read the file
        let mut cfg_string = String::new();
        file.read_to_string(&mut cfg_string)
            .wrap_err("Failed to read config file")?;
        let config = toml::from_str(&cfg_string).wrap_err("Failed to serialize configuration")?;
        Ok(ConfigFile { file, config })
    }

    /// Write the default config to a given file
    #[instrument]
    pub fn write_default(file: File) -> Result<ConfigFile> {
        // Write the default config
        let config = Config::default();
        let mut cfg = ConfigFile { file, config };
        cfg.update_file()?;
        Ok(cfg)
    }

    /// Updates the stored config file
    #[instrument(skip(self))]
    pub fn update_file(&mut self) -> Result<()> {
        info!("Updating config");
        let cfg_string =
            toml::to_string_pretty(&self.config).wrap_err("Failed to serialize configuration")?;
        self.file
            .seek(SeekFrom::Start(0))
            .wrap_err("Failed to seek in file")?;
        self.file
            .write_all(cfg_string.as_bytes())
            .wrap_err("Failed to write config file")?;
        Ok(())
    }

    /// Reload the configuration from disk
    #[instrument(skip(self))]
    pub fn reload_config(&mut self) -> Result<()> {
        info!("Reloading config file");
        let mut cfg_string = String::new();
        self.file
            .seek(SeekFrom::Start(0))
            .wrap_err("Failed to seek in file")?;
        self.file
            .read_to_string(&mut cfg_string)
            .wrap_err("Failed to read from config file")?;
        let cfg = toml::from_str(&cfg_string).wrap_err("Failed to deserialize config")?;
        self.config = cfg;
        Ok(())
    }
}
