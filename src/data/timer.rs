use std::collections::HashSet;

use eyre::Result;
use matrix_sdk::{async_trait, room::Room, ruma::RoomId};
use tracing::{debug, info, instrument, trace, warn};

use crate::Turtle;

/// Trait for repeating actions
#[async_trait]
pub trait Timer: Send + Sync + 'static {
    /// Action that gets called when the timer is executed.
    async fn call(&mut self, turtle: &Turtle) -> Result<()>;
    /// Human readable name for the timer
    fn name(&self) -> String;
}

/// A timer that, when called, will automatically walk each space indicated in the turtle's
/// configuration, and make sure that the turtle is joined to every reachable public room in the space.
pub struct SpaceAutoJoin;

#[async_trait]
impl Timer for SpaceAutoJoin {
    #[instrument(skip(self, turtle))]
    async fn call(&mut self, turtle: &Turtle) -> Result<()> {
        for space in turtle.get_spaces().await? {
            info!(?space, "Verifying membership for all rooms");
            let results: HashSet<RoomId> = turtle.get_hierarchy(&space).await?.room_ids();
            for room in results {
                trace!(?room, "Checking membership in room");
                let joined = matches!(turtle.client.get_room(&room), Some(Room::Joined(_)));
                trace!(?joined);
                if !joined {
                    if turtle.is_room_public(&room).await? {
                        debug!(?room, "Attempting to join room");
                        let result = turtle.client.join_room_by_id(&room).await;
                        if let Err(e) = result {
                            warn!(?e, ?room, "Failure joining room");
                        }
                    } else {
                        trace!(?room, "Room is non-public");
                    }
                }
            }
        }
        Ok(())
    }

    fn name(&self) -> String {
        "Space auto-joiner".to_string()
    }
}
