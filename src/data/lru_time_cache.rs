use std::{
    collections::HashMap,
    fmt::Debug,
    hash::Hash,
    mem::{self, MaybeUninit},
    time::{Duration, Instant},
};

use tracing::{instrument, trace};

/// LRU + Time based cache
pub struct Cache<K, V, const N: usize> {
    /// Storage for the keys in the map
    keys: HashMap<K, usize>,
    /// Storage for the values in the map
    values: [Option<V>; N],
    /// Time each value was last used at
    last_used: [Option<Instant>; N],
    /// Max duration to keep value in the queue for
    ttl: Duration,
}

impl<K, V, const N: usize> Cache<K, V, N>
where
    K: Hash + Debug + Eq + Clone,
    V: Debug,
{
    /// Creates a new, empty, cache
    #[instrument]
    pub fn new(ttl: Duration) -> Self {
        // Its really stupid but this is the only way to do this generically
        let mut values: [MaybeUninit<Option<V>>; N] =
            unsafe { MaybeUninit::zeroed().assume_init() };
        for elem in &mut values {
            elem.write(None);
        }
        let values: [Option<V>; N] = unsafe {
            let value_pointer: *mut [MaybeUninit<Option<V>>; N] = &mut values;
            mem::forget(values);
            let value_pointer: *mut [Option<V>; N] = value_pointer.cast();
            value_pointer.read()
        };
        trace!("Initialized array");
        Self {
            keys: HashMap::new(),
            values,
            last_used: [None; N],
            ttl,
        }
    }

    /// Returns the first empty slot
    #[instrument(skip(self))]
    fn first_empty_slot(&self) -> Option<usize> {
        for i in 0..N {
            // Validation check
            debug_assert_eq!(self.values[i].is_none(), self.last_used[i].is_none());
            if self.values[i].is_none() {
                trace!(?i, "Found empty slot");
                return Some(i);
            }
        }
        trace!("No empty slots found");
        None
    }

    /// Removes the value with the specified index
    fn get_key_at_index(&self, index: usize) -> Option<K> {
        for (k, v) in &self.keys {
            if index == *v {
                return Some(k.clone());
            }
        }
        None
    }

    /// Evicts a key at the given index
    fn evict_at_index(&mut self, index: usize) -> Option<(K, V)> {
        let key = self.get_key_at_index(index)?;
        self.keys.remove(&key);
        let value = self.values[index].take().unwrap();
        self.last_used[index] = None;
        Some((key, value))
    }

    /// Evicts entries that are too old
    #[instrument(skip(self))]
    fn evict_old(&mut self) {
        let current_time = Instant::now();
        let mut indicies = vec![];
        for (i, value) in self.last_used.iter_mut().enumerate() {
            if let Some(time) = *value {
                let duration = current_time.duration_since(time);
                if duration > self.ttl {
                    indicies.push((i, duration));
                }
            }
        }
        for (index, duration) in indicies {
            let (k, v) = self.evict_at_index(index).expect("Constraint violated");
            trace!(?k, ?v, ?duration, "Evicted key due to age");
        }
    }

    /// Evicts least recently used entry
    fn evict_lru(&mut self) {
        let min_index = self
            .last_used
            .iter()
            .enumerate()
            .filter(|(_, x)| x.is_some())
            .map(|(i, x)| (i, x.unwrap()))
            .min_by(|(_, x), (_, y)| x.cmp(y));
        if let Some((index, instant)) = min_index {
            let duration = Instant::now().duration_since(instant);
            let (k, v) = self.evict_at_index(index).expect("Constraints violated");
            trace!(?k, ?v, ?duration, "Evicted least recently used key");
        }
    }

    /// Inserts an item into the cache
    #[instrument(skip(self, value))]
    pub fn insert(&mut self, key: K, value: V) {
        trace!("Entering insert");
        if let Some(index) = self.keys.get(&key) {
            // Update value in place
            trace!("Updating key in place");
            self.values[*index] = Some(value);
            self.last_used[*index] = Some(Instant::now());
        } else if let Some(index) = self.first_empty_slot() {
            // Insert key in fresh, already empty slot
            trace!(?index, "Inserting key into new slot");
            self.keys.insert(key, index);
            self.values[index] = Some(value);
            self.last_used[index] = Some(Instant::now());
        } else {
            trace!("Attempting eviction of old entries to make room");
            self.evict_old();
            if let Some(index) = self.first_empty_slot() {
                trace!(?index, "Deleting old entries made space, inserting");
                self.keys.insert(key, index);
                self.values[index] = Some(value);
                self.last_used[index] = Some(Instant::now());
            } else {
                trace!("No old entries to cull, culling lru");
                self.evict_lru();
                let index = self.first_empty_slot().expect("Somehow failed to cull LRU");
                self.keys.insert(key, index);
                self.values[index] = Some(value);
                self.last_used[index] = Some(Instant::now());
            }
        }
    }

    /// Returns a reference to the value for the supplied key, if the cache has it, and it has not
    /// exceed its time to live
    #[instrument(skip(self))]
    pub fn get(&mut self, key: &K) -> Option<&V> {
        let index = *self.keys.get(key)?;
        // Check to make sure this entry is still fresh
        if Instant::now().duration_since(self.last_used[index].expect("Constraints violated"))
            > self.ttl
        {
            let (k, v) = self.evict_at_index(index).expect("Constraints violated");
            trace!(?k, ?v, "Stale value evicted");
            None
        } else {
            // Update the last_used time
            self.last_used[index] = Some(Instant::now());
            Some(self.values[index].as_ref().expect("Constraints violated"))
        }
    }

    /// Returns true if this cache contains an up-to-date entry for the given key
    #[instrument(skip(self))]
    pub fn contains_key(&mut self, key: &K) -> bool {
        self.get(key).is_some()
    }
}
