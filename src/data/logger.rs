use eyre::Result;
use matrix_sdk::{
    ruma::{
        events::room::message::{MessageEventContent, MessageType, TextMessageEventContent},
        RoomId,
    },
    Client,
};
use tracing::{instrument, trace};

/// Keeps track of logging methods and the like for the [`Turtle`](crate::Turtle)
#[derive(Debug)]
pub struct Logger {
    /// sdk client used to log to a channel with
    client: Client,
    /// Room to log to
    log_channel: Option<RoomId>,
}

impl Logger {
    /// Create a new logger
    pub fn new(client: Client, log_channel: Option<RoomId>) -> Self {
        Self {
            client,
            log_channel,
        }
    }

    /// Logs the message to all available outputs
    ///
    /// # Errors
    ///
    /// Will pass through the underlying error if writing to any of the rooms, files, etc. fails.
    #[instrument(skip(message), err)]
    pub async fn log_message(&self, message: impl AsRef<str>) -> Result<()> {
        let message = message.as_ref().to_string();
        // send message to the logging channel
        trace!(%message);
        if let Some(id) = &self.log_channel {
            let tmec = TextMessageEventContent::markdown(message);
            let mt = MessageType::Text(tmec);
            let mec = MessageEventContent::new(mt);

            self.client.room_send(id, mec, None).await?;
        }
        Ok(())
    }
}
