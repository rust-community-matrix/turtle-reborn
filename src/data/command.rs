use std::fmt::Debug;

use eyre::Result;
use matrix_sdk::{
    async_trait,
    room::{Joined, Room},
    ruma::{
        events::{
            room::message::{MessageEventContent, MessageType},
            SyncMessageEvent,
        },
        UserId,
    },
};
use tracing::{info, instrument, trace};

use crate::Turtle;

pub mod commands;

/// Raw, unparsed command
#[derive(Debug)]
pub struct RawCommand {
    /// Prefix the command was invoked with
    pub prefix: Option<String>,
    /// Command invoked
    pub command: String,
    /// Unsplit arguments
    pub args: String,
    /// Indicates if this command was sent via DM
    pub direct_message: bool,
    /// Room-local permissions level of the sender
    pub permission_level: i64,
    /// The user sending the command
    pub user: UserId,
    /// Room this command was issued in
    pub room: Joined,
    /// Raw event that triggered this command
    pub event: SyncMessageEvent<MessageEventContent>,
}

/// Permission levels for a `Command`
pub enum PermissionsLevel {}

/// Struct for parsing commands
#[derive(Debug)]
pub struct CommandParser {
    /// Supported prefixes
    prefixes: Vec<String>,
    /// List of `Command`s
    pub(crate) commands: Vec<Box<dyn Command>>,
}

impl CommandParser {
    /// Creates a new `CommandParser` with the given prefixes
    pub fn new(prefixes: impl IntoIterator<Item = impl AsRef<str>>) -> Self {
        let prefixes = prefixes
            .into_iter()
            .map(|x| x.as_ref().to_string())
            .collect();
        Self {
            prefixes,
            commands: vec![],
        }
    }

    /// Adds a new parser to this `CommandParser`
    pub fn add_command(&mut self, command: impl Command + 'static) {
        let command = Box::new(command);
        self.commands.push(command);
    }

    /// Parses a command into a raw command
    #[instrument(skip(self, event))]
    pub async fn parse_command(
        &self,
        event: SyncMessageEvent<MessageEventContent>,
        room: &Room,
        direct_message: bool,
    ) -> Option<RawCommand> {
        trace!("Attempting to parse raw command");
        // The room must be one we have joined
        if let Room::Joined(room) = room {
            trace!(
                "Encrypted: {:?}, Encryption settings: {:?}",
                room.is_encrypted(),
                room.encryption_settings()
            );
            trace!(?direct_message, "In joined room");
            // Commands can only be in the form of a text message
            if let MessageType::Text(text_message) = &event.content.msgtype {
                trace!(?text_message, "Message was text");
                let text_message: &str = text_message.body.trim_start();
                // Accept the command without a prefix if it is in a dm
                let (prefix, trimmed_msg) = if let Some((prefix, trimmed_msg)) =
                    try_prefixes(&self.prefixes, text_message)
                {
                    (Some(prefix), trimmed_msg)
                } else if direct_message {
                    (None, text_message)
                } else {
                    return None;
                };
                trace!("Parsed prefix");

                let (command, args) = split_command(trimmed_msg)?;
                trace!("Split command and args");
                let user = event.sender.clone();
                let permission_level = room.get_member(&user).await.ok().flatten()?.power_level();
                trace!("Grabbed power level");

                let raw_command = RawCommand {
                    prefix,
                    command: command.to_string(),
                    args: args.to_string(),
                    direct_message,
                    permission_level,
                    user,
                    room: room.clone(),
                    event,
                };
                Some(raw_command)
            } else {
                None
            }
        } else {
            None
        }
    }

    /// Attempts to execute a raw command
    #[instrument(skip(self, turtle, raw_command))]
    pub async fn execute_command(
        &self,
        raw_command: RawCommand,
        turtle: &Turtle,
    ) -> Option<Result<()>> {
        for command in &self.commands {
            if command.matches(&raw_command) {
                info!(?command, "Evaluating command");
                return Some(command.execute(raw_command, turtle).await);
            }
        }
        None
    }
}

/// Trait for commands
#[async_trait]
pub trait Command: Debug + Send + Sync {
    /// Returns true if the provided raw command is an invocation of this `Command`
    fn matches(&self, command: &RawCommand) -> bool;
    /// Attempts to execute the command
    async fn execute(&self, command: RawCommand, turtle: &Turtle) -> Result<()>;
}

/// Utility function, returns a pair of the first prefix the message matches, if any, and the
/// message with the prefixes stripped
fn try_prefixes(
    prefixes: impl IntoIterator<Item = impl AsRef<str>>,
    message: &str,
) -> Option<(String, &str)> {
    let iter = prefixes.into_iter().map(|x| x.as_ref().to_string());
    for prefix in iter {
        if message.starts_with(&prefix) {
            let ret = message.strip_prefix(&prefix)?;
            return Some((prefix, ret));
        }
    }
    None
}

/// Utility function, splits the post-prefix portion of a command into its command and arguments
/// strings
fn split_command(command: &str) -> Option<(&str, &str)> {
    let command = command.trim();
    if command.is_empty() {
        None
    } else if let Some((prefix, suffix)) = command.split_once(' ') {
        Some((prefix.trim(), suffix.trim()))
    } else {
        Some((command, ""))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_try_prefixes() {
        let prefixes = ["!", "-", "turtle, please "];
        assert_eq!(
            try_prefixes(&prefixes, "!test"),
            Some(("!".to_string(), "test"))
        );
        assert_eq!(
            try_prefixes(&prefixes, "-aest"),
            Some(("-".to_string(), "aest"))
        );
        assert_eq!(
            try_prefixes(&prefixes, "turtle, please best"),
            Some(("turtle, please ".to_string(), "best"))
        );
        assert_eq!(try_prefixes(&prefixes, "@cest"), None);
    }

    #[test]
    fn test_split_command() {
        assert_eq!(split_command(""), None);
        assert_eq!(split_command("   "), None);
        assert_eq!(split_command("do the thing"), Some(("do", "the thing")));
        assert_eq!(split_command("do"), Some(("do", "")));
    }
}
