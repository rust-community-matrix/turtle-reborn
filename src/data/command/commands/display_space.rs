use std::convert::TryFrom;

use eyre::{ensure, eyre, Context, ContextCompat, Result};
use matrix_sdk::{
    async_trait,
    ruma::{
        events::room::message::{MessageEventContent, MessageType, TextMessageEventContent},
        RoomAliasId, RoomId,
    },
};
use tracing::{debug, instrument, warn};

use crate::{
    data::command::{Command, RawCommand},
    space_walker::{format_node, walk_space_raw},
    Turtle,
};

/// Walks a space, and formats it as markdown for the user to see.
#[derive(Debug)]
pub struct DisplaySpace {
    /// Controls filtering of pipe (`|`) separated topics.
    ///
    /// If true, the following filters will be applied
    ///   * A | B | C -> B
    ///   * A | B -> A
    pub pipe_splice: bool,
    /// Prefixes to filter out of space topics
    pub filtered_prefixes: Vec<&'static str>,
}

#[async_trait]
impl Command for DisplaySpace {
    fn matches(&self, command: &RawCommand) -> bool {
        command.command.to_lowercase() == "display_space"
    }

    #[instrument(skip(command, turtle), err)]
    async fn execute(&self, command: RawCommand, turtle: &Turtle) -> Result<()> {
        ensure!(
            self.matches(&command),
            eyre!("Provided command was not a display_space")
        );
        // Make sure the user has the ability to use the `display_space` command, which may only be run if:
        //   * The user is a bot admin
        //   * The user has elevated permissions in the room
        //   * The room is a DM
        let has_permission = turtle.get_admins().await.contains(&command.user)
            || command.permission_level > 10
            || command.direct_message;
        if has_permission {
            // get the room id for the space
            let room_id = if let Ok(room_id) = RoomId::try_from(command.args.clone()) {
                debug!("Using direct room id");
                room_id
            } else {
                debug!("Using room alias");
                let room_alias_id =
                    RoomAliasId::try_from(command.args).wrap_err("Failed to parse space")?;
                turtle
                    .get_room_id(&room_alias_id)
                    .await?
                    .context("No such room")?
            };

            // attempt to use the directory first
            match turtle.get_hierarchy(&room_id).await {
                Ok(mut node) => {
                    if self.pipe_splice {
                        node = node.map(crate::space_directory::pipe_split);
                    }
                    node = node.map(crate::space_directory::exclude_lines(
                        &self.filtered_prefixes,
                        false,
                    ));
                    node = node
                        .filter(crate::space_directory::only_public)
                        .wrap_err("Attempted to explore a private space")?;
                    let markdown_body = node.markdown_format();
                    let tmec = TextMessageEventContent::markdown(markdown_body);
                    let mt = MessageType::Text(tmec);
                    let mec = MessageEventContent::new(mt);
                    command.room.send(mec, None).await?;
                    Ok(())
                }
                // Fallback on manual walking
                Err(e) => {
                    warn!(?e, "Error using directory, falling back to walking");
                    turtle
                        .log_message(format!(
                            "Failed to look up space {}\n```\n{:#?}\n```",
                            room_id, e
                        ))
                        .await?;

                    let results = walk_space_raw(turtle.clone(), room_id.clone()).await?;
                    let entry_node = if let Some(entry_node) =
                        results.iter().find(|p| p.room_id_eq(&room_id)).cloned()
                    {
                        entry_node
                    } else {
                        warn!(?results, ?room_id, "Failed to find space in results");
                        return Err(eyre!("display_space failed!"));
                    };
                    let markdown_body = format_node(
                        turtle.clone(),
                        entry_node,
                        0,
                        &results,
                        self.pipe_splice,
                        self.filtered_prefixes.clone(),
                    )
                    .await;
                    if let Some(markdown_body) = markdown_body {
                        let tmec = TextMessageEventContent::markdown(markdown_body);
                        let mt = MessageType::Text(tmec);
                        let mec = MessageEventContent::new(mt);
                        command.room.send(mec, None).await?;
                        Ok(())
                    } else {
                        warn!("Attempted to walk a private space?");
                        Ok(())
                    }
                }
            }
        } else {
            let message = format!("{}: You do not have permission to use this command here. You need elevated permissions to use it outside of a DM", command.user);
            command
                .room
                .send(MessageEventContent::text_plain(message), None)
                .await?;
            Ok(())
        }
    }
}
