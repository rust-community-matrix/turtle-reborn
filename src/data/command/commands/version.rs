use eyre::{ensure, eyre, Result};
use matrix_sdk::{
    async_trait,
    ruma::events::{room::message::MessageEventContent, AnyMessageEventContent},
};
use tracing::instrument;

use crate::{
    data::command::{Command, RawCommand},
    Turtle,
};

/// Displays version information of the currently running bot
#[derive(Debug)]
pub struct VersionInfo;

#[async_trait]
impl Command for VersionInfo {
    fn matches(&self, command: &RawCommand) -> bool {
        command.command.to_lowercase() == "version"
    }

    #[instrument(skip(command, turtle), err)]
    async fn execute(&self, command: RawCommand, turtle: &Turtle) -> Result<()> {
        ensure!(
            self.matches(&command),
            eyre!("Provided command was not !version"),
        );

        let config = turtle.config_file.read().await;
        // Get the source url
        let source_url: &str = &config.config.source_link;
        // Get the homeserver url
        let homeserver_url: &str = config.config.login.homeserver.as_str();

        // Generate version string
        let version_string = format!(
            "Turtle Reborn v{} \nSource: {}\nHomeserver: {}",
            env!("CARGO_PKG_VERSION"),
            source_url,
            homeserver_url
        );

        command
            .room
            .send(
                AnyMessageEventContent::RoomMessage(MessageEventContent::text_plain(
                    &version_string,
                )),
                None,
            )
            .await?;

        Ok(())
    }
}
