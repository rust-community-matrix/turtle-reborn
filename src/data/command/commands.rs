//! Implementation of included commands

use eyre::{ensure, eyre, Result};
use matrix_sdk::{async_trait, ruma::events::room::message::MessageEventContent};
use tracing::instrument;

use crate::{
    data::command::{Command, RawCommand},
    Turtle,
};

/// Space walking command
pub mod display_space;
/// Displays version information for the bot
pub mod version;

pub use display_space::DisplaySpace;
pub use version::VersionInfo;

/// Basic ping command
///
/// Echos back what the user said, prefixed with their display name and user name, to prevent exploiting
/// the command to send anonymous messages.
#[derive(Debug)]
pub struct Ping;

#[async_trait]
impl Command for Ping {
    fn matches(&self, command: &RawCommand) -> bool {
        command.command.to_lowercase() == "ping"
    }

    #[instrument(skip(self, turtle))]
    async fn execute(&self, command: RawCommand, turtle: &Turtle) -> Result<()> {
        ensure!(
            self.matches(&command),
            eyre!("Provided command was not a ping")
        );
        let reply = command.args.trim();
        let message = if reply.is_empty() {
            format!("Pong! ({})", command.user)
        } else if let Ok(display_name) = turtle
            .get_display_name(Some(command.room.room_id()), &command.user)
            .await
        {
            format!("{}({}): {}", display_name, command.user, reply)
        } else {
            format!("{}: {}", command.user, reply)
        };

        command
            .room
            .send(MessageEventContent::text_plain(message), None)
            .await?;
        Ok(())
    }
}
