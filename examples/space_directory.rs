use std::{convert::TryFrom, fs::OpenOptions};

use directories::ProjectDirs;
use eyre::{eyre, ContextCompat, Result, WrapErr};
use matrix_sdk::ruma::{RoomAliasId, RoomId};
use structopt::StructOpt;
use tracing::{debug, info, instrument};

use turtle_reborn::{
    config::ConfigFile,
    space_directory::{exclude_lines, only_public, pipe_split},
    Turtle,
};

#[derive(StructOpt, Debug)]
#[structopt(
    name = "walk_space",
    about = "Walks and space and dumps it into a human readable format"
)]
struct Opt {
    /// RoomAliasId of the space to explore
    #[structopt(name = "SPACE")]
    space: String,
}

#[tokio::main]
async fn main() -> Result<()> {
    // Setup tracing subscriber
    tracing_subscriber::fmt()
        .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
        .pretty()
        .init();
    // Read options
    let opt = Opt::from_args();
    // Read config file
    let config = open_config()?;
    // Open the turtle
    let turtle = Turtle::new(config, ["!"]).await?;

    // get the room id for the space
    let room_id = if let Ok(room_id) = RoomId::try_from(opt.space.clone()) {
        debug!("Using direct room id");
        room_id
    } else {
        debug!("Using room alias");
        let room_alias_id = RoomAliasId::try_from(opt.space).wrap_err("Failed to parse space")?;
        turtle
            .get_room_id(&room_alias_id)
            .await?
            .context("No such room")?
    };
    let x = turtle.get_hierarchy(&room_id).await?;
    let x = x
        .map(pipe_split)
        .map(exclude_lines(
            ["Read the rules:", "Information and faqs:"],
            false,
        ))
        .filter(only_public)
        .unwrap();

    println!("{}", x.markdown_format());
    Ok(())
}

/// Open an existing config file
#[instrument]
fn open_config() -> Result<ConfigFile> {
    let config_dir = ProjectDirs::from("rs", "community", "TurtleReborn")
        .wrap_err("Failed to find config dir")?
        .config_dir()
        .to_path_buf();
    info!("Configuration directory is {:?}", config_dir);
    let config_path = config_dir.join("config.toml");
    info!("Configuration path is {:?}", config_path);
    let file = OpenOptions::new()
        .read(true)
        .write(false)
        .create(false)
        .open(&config_path)
        .wrap_err("Failed to open config file")?;
    let config_file = ConfigFile::read(file)?;
    if config_file.config.configured {
        Ok(config_file)
    } else {
        Err(eyre!("Instance has not been configured"))
    }
}
