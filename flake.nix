{
  description = "The turtle-reborn matrix utility bot";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    crate2nix = {
      url = "github:kolloch/crate2nix";
      flake = false;
    };
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-compat, utils, crate2nix, fenix }:
    utils.lib.eachDefaultSystem (system:
      let
        fenixPackage = fenix.packages.${system}.stable.withComponents [ "cargo" "clippy" "rust-src" "rustc" "rustfmt"];
        rustOverlay = final: prev:
          {
            inherit fenixPackage;
            rustc = fenixPackage;
            cargo = fenixPackage;
            rust-src = fenixPackage;
          };

        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            rustOverlay
          ];
        };

        # DON'T FORGET TO PUT YOUR PACKAGE NAME HERE, REMOVING `throw`
        crateName = "turtle-reborn";

        inherit (import "${crate2nix}/tools.nix" { inherit pkgs; })
          generatedCargoNix;

        project = import
          (generatedCargoNix {
            name = crateName;
            src = ./.;
          })
          {
            inherit pkgs;
            defaultCrateOverrides = pkgs.defaultCrateOverrides // {
              # Crate dependency overrides go here
              olm-sys = attrs: {
                nativeBuildInputs = [ pkgs.cmake ];
                buildInputs = [ pkgs.olm ];
                OLM_LINK_VARIANT = "dylib";
              };
            };
          };

      in
      {
        packages.${crateName} = project.rootCrate.build;
        packages.tests.${crateName} = project.rootCrate.build.override {
          runTests = true;
        };

        defaultPackage = self.packages.${system}.${crateName};

        devShell = pkgs.mkShell {
          inputsFrom = builtins.attrValues self.packages.${system};
          buildInputs =
            with pkgs; [ cargo-audit nixpkgs-fmt git-chglog openssl pkgconfig fenix.packages.${system}.rust-analyzer cmake fenixPackage ];
        };
      });
}
